@extends('layouts.panelbase')



    @section('content')


      <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Pages</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
          <div class="btn-group mr-2">

            <a href="{{ route('addpage') }}" class="btn btn-info">Añadir</a>
          </div>
        </div>
      </div>

      <div class="table-responsive white-back">
        <table class="table table-striped table-sm">
          <thead>
            <tr>
              <th>Id</th>
              <th>Título</th>
              <th>Fecha</th>
              <th></th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            @if($pages->count())  
              @foreach($pages as $item)  
              <tr>
                <td>{{ $item->id }}</td>
                <td>{{ $item->title }}</td>
                <td>{{ $item->created_at }}</td>
                <td><a class="btn btn-sm btn-info" href="#" >Editar</a></td>
                <td><a class="btn btn-sm btn-danger" href="#" >Borrar</a></td>
              </tr>
              @endforeach 
               @else
               <tr>
                <td colspan="8">No hay registro !!</td>
              </tr>
            @endif
          </tbody>
        </table>
      </div>



    
@endsection

