@extends('layouts.frontbase')

@section('title', $pack->name)

@section('content')
<div class="album py-5 bg-light">
    <div class="container text-center pt-5">

        <div class="row tariff-back">
			<div class="col-md-6">
				<img src="{{ asset('img/' . $pack->image) }}" class="img-fluid" alt="{{ $pack->name }}" />
			</div>

			<div class="col-md-6">

				<h1>{{ $pack->name }}</h1>

				<div class="price">
					{{ $pack->price / 100 }} €
				</div>

				<div class="text-left m-3">
					{{ $pack->short_description }}


					
				</div>

				<div class="text-left m-3">
					<a href="{{ route('alcarrito', ['type' => 'pack', 'product' => $pack->slug]) }}" class="btn btn-info">Contratar</a>
				</div>


				<p class="description">
					{!!nl2br($pack->description)!!}
				</p>

			</div>
        </div>
    </div>

</div>

@endsection