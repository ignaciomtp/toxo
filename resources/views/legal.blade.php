@extends('layouts.frontbase')

@section('title', $legalText->title)

@section('content')
<div class="album py-5 bg-light">
    <div class="container ">
        <h1>{{ $legalText->title }}</h1>

        <p class="description">
			{!!nl2br($legalText->body)!!}
        </p>

    </div>

</div>

@endsection