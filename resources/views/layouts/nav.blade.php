<nav class="navbar navbar-expand-md navbar-light fixed-top nav-back ">
  <a class="navbar-brand" href="{{ route('inicio') }}">
    <img src="{{ asset('img/logo.jpg') }}" alt="Toxo Telecom" />
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarsExampleDefault">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item ">
        <a class="nav-link" href="{{ route('inicio') }}">Inicio </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('tipo', ['type' => 'movil']) }}" >Tarifas Móvil</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('tipo', ['type' => 'adsl']) }}" >Fibra Óptica</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('packs') }}" >Packs</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('ofertas') }}" >Ofertas</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('noticias') }}" >Noticias</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('acerca') }}">Acerca de Toxo</a>
      </li>
    </ul>
    <div class="form-inline my-2 my-lg-0">
      <a href="{{ route('carrito') }}" class="btn btn-info mr-3"><i class="fas fa-shopping-cart"></i> Solicitudes ({{ $cartItems }})</a>
      <a href="https://clientestoxotelecom.ispgestion.com/site/login" class="btn btn-outline-success my-2 my-sm-0" >Toxeira</a>
    </div>
  </div>
</nav>