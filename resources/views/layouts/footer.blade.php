
<footer class="page-footer font-small pt-4">


  <div class="container-fluid text-center text-md-left">

    <!-- Grid row -->
    <div class="row">

      <hr class="clearfix w-100 d-md-none pb-3">


      <div class="col-md-3 mb-md-0 mb-3">

        <h5 class="text-uppercase">TOXO TELECOM</h5>

        <ul class="list-unstyled">
          @if(isset($customPages))
            @foreach($customPages as $page)
              <li>
                <a href="{{ route('custompage', ['page' => $page->slug]) }}">{{ $page->title }}</a>
              </li>
            @endforeach
          @endif

      </div>
      <!-- Grid column -->

      <!-- Grid column -->
      <div class="col-md-3 mb-md-0 mb-3">

        <!-- Links -->
        <h5 class="text-uppercase">LEGAL</h5>

        <ul class="list-unstyled">
          @if(isset($legalSlugs))
            @foreach($legalSlugs as $ls)
              <li>
                  <a href="{{ route('legal', ['text' => $ls->slug]) }}">{{ $ls->title }}</a>
              </li>
            @endforeach
          @endif
        </ul>

      </div>
      <!-- Grid column -->

      <div class="col-md-6 mt-md-0 mt-3">

        <a href="https://www.facebook.com/toxotelecomunicaciones/" target="_blank" id="facebook" >
          <i class="fab fa-facebook-square fa-3x"></i>
        </a>

        <a href="https://twitter.com/toxotelecom?lang=es" target="_blank" id="twitter" >
          <i class="fab fa-twitter-square fa-3x"></i>
        </a>

        <a href="https://www.instagram.com/toxotelecom/" target="_blank" id="instagram" >
          <i class="fab fa-instagram-square fa-3x"></i>
        </a>

      </div>

    </div>
    <!-- Grid row -->

  </div>

  <!-- Copyright -->
  <div class="footer-copyright text-center py-3">© 2020 Copyright:
    <a href="https://ctbarbanza.com/"> ctBarbanza</a>
  </div>
  <!-- Copyright -->

</footer>
