@extends('layouts.frontbase')

@section('content')
<div class="album py-5 bg-light">
    <div class="container text-center pt-5">

        <h1>Packs</h1>

        <div class="row ">

			@foreach($packs as $pack)
                <div class="col-md-3">
                  <div class="card mb-4 shadow-sm">
                    <a href="{{ route('pack', ['pack' => $pack->slug]) }}" >
                        <div class="card-header">
                            {{ strtoupper ($pack->name) }}
                        </div>

                         <div class="card-body ">
                            <div class="desc">

                                <div class="row">
                                    <div class="desc-half tm">
                                        {{ $pack->mobileTariff->name }}
                                    </div>
                                    <div class="desc-half separator ">
                                       	{{ $pack->adslTariff->name }}
                                    </div>
                                </div>

                            </div>
                            <div class="price">
                                {{ $pack->price / 100 }}
                            </div>

                            <div class="iva">
                                € / mes IVA inc.
                            </div>
                         </div>
                    </a>

                  </div>
                </div>
            @endforeach
        </div>

    </div>

</div>

@endsection