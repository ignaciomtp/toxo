@extends('layouts.frontbase')

@section('title', 'Inicio')

@section('content')
<div class="album py-5 bg-light">
    <div class="container text-center pt-2">

        <div id="toxoCarousel" class="carousel slide mb-2" data-ride="carousel">

              <div class="carousel-inner">
                @foreach($slides as $key => $slide)
                <div class="carousel-item @if($key == 0) active @endif">
                    <a href="{{ url($slide->link) }}">
                        <img class="d-block w-100" src="{{ asset('img/' . $slide->image) }}" alt="{{ $slide->title }}" />
                    </a> 
                </div>
                @endforeach


              </div>

              <a class="carousel-control-prev" href="#toxoCarousel" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="carousel-control-next" href="#toxoCarousel" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
        </div>

        <h1>Productos destacados</h1>
        <div class="row ">
            @foreach($tariffs as $tariff)
                <div class="col-md-3">
                  <div class="card mb-4 shadow-sm">
                    <a href="{{ route('tarifa', ['type' => $tariff->type->slug, 'tariff' => $tariff->slug]) }}" >
                        <div class="card-header">
                            {{ strtoupper ($tariff->name) }}
                        </div>

                         <div class="card-body ">
                            <div class="desc">
                                @if($tariff->tariff_type_id == 1)
                                <div class="row">
                                    <div class="desc-half tm">
                                        {{ $tariff->bandwidth }} {{ $tariff->units }}
                                    </div>
                                    <div class="desc-half separator ">
                                        @if($tariff->minutes == null)
                                            Llamadas Ilimitadas
                                        @else
                                            {{ $tariff->minutes }} Minutos
                                        @endif
                                    </div>
                                </div>
                                @else
                                    <div class="fo">
                                        {{ $tariff->bandwidth }} {{ $tariff->units }}
                                    </div>
                                @endif
                            </div>
                            <div class="price">
                                {{ $tariff->price / 100 }}
                            </div>

                            <div class="iva">
                                € / mes IVA inc.
                            </div>
                         </div>
                    </a>

                  </div>
                </div>
            @endforeach

        </div>

        <div class="mt-3 mb-3 sombra">
            <img src="{{ asset('img/sertoxo.png') }}" class="img-fluid" alt="Ser un Toxo xa non é o que era" />

        </div>

    </div>
</div>
@endsection
