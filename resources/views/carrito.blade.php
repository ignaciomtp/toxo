@extends('layouts.frontbase')

@section('content')
<div class="album py-5 bg-light">
    <div class="container text-center mb-4">

    	<div class="row text-left">
			<div class="col-8 product-back sombra p-0">
				<h2 class="m-3">Solicitudes</h2>

				<hr />

				@if(isset($product))

					<div class="row m-3">
						<div class="col-3">
							<img src="{{ asset('img/' . $product->image ) }}" class="img-fluid" alt="{{ $product->name }}" />
						</div>
						<div class="col-6">
							{{ $product->name }}
						</div>
						<div class="col-2">
							{{ $product->price / 100 }} €
						</div>
						<div class="col-1">
							<a href="{{ route('quitar') }}"><i class="fas fa-trash-alt"></i> </a>
						</div>
					</div>
				@else 
					<div class="row m-3">
						No hay solicitudes
					</div>
				@endif
			</div>

			<div class="col-3 offset-1 product-back sombra p-0">
				<div class="m-3">
					<div class="row">
						<div class="col-8">
							Cuota mensual
						</div>
						<div class="col-4 text-right">
							@if(isset($product))
								{{ $product->price / 100 }} €
							@else 
								0.00 €
							@endif
						</div>
					</div>
					<div class="row">
						<div class="col-8">
							Tarjeta SIM
						</div>
						<div class="col-4 text-right">
							Gratis
						</div>
					</div>

					<p class="mt-3 mb-3">
	La tarjeta SIM de Toxo es gratuita. Puede recogerse en tienda o enviarse a domicilio. En caso de enviarse, se añade en la primera factura un importe de 6 euros asociados al coste de envío.
					</p>

				</div>

				<hr />
					<div class="row m-2">
						<div class="col-8">
							Total (impuestos inc.)
						</div>
						<div class="col-4 text-right">
							@if(isset($product))
								{{ $product->price / 100 }} €
							@else 
								0.00 €
							@endif
						</div>
					</div>
				<hr />

				<div class="m-3 text-center">
					@if(isset($product))
						<a href="{{ route('alta') }}" class="btn btn-info">Solicitar</a>
					@else 
						<a href="#" class="btn btn-info disabled">Solicitar</a>
					@endif
					
				</div>
				
			</div>
    	</div>
        

    </div>

</div>

@endsection