@extends('layouts.frontbase')

@section('title', $post->title)

@section('content')
<div class="album py-5 bg-light">
    <div class="container text-center">
        

        <div class="page-list text-left">
			<h1>{{ $post->title }}</h1>
			
			<hr />

			<div class="post-date">
				{{ $post->date }}
			</div>

        	<img src="{{ asset('img/' . $post->image) }}" class="img-fluid" alt="{{ $post->title }}" />

			{!! $post->body !!}

        </div>

    </div>

</div>

@endsection