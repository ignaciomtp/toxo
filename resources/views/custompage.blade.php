@extends('layouts.frontbase')

@section('title', $page->title)

@section('content')
<div class="album py-5 bg-light">
    <div class="container ">
        <h1>{{ $page->title }}</h1>

        <div class="content-page product-back p-3">
		
			{!! $page->body !!}

        </div>

    </div>

</div>

@endsection