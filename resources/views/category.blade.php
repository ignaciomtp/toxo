@extends('layouts.frontbase')

@section('content')
<div class="album py-5 bg-light">
    <div class="container text-center pt-5">

        <h1>{{ $typeTariff->name }}</h1>

        <div class="row ">
            @foreach($tariffs as $tariff)
                <div class="col-md-3">
                  <div class="card mb-4 shadow-sm">
                    <a href="{{ route('tarifa', ['type' => $tariff->type->slug, 'tariff' => $tariff->slug]) }}" >
                        <div class="card-header">
                            {{ strtoupper ($tariff->name) }}
                        </div>

                         <div class="card-body ">
                            <div class="desc">
                                @if($tariff->tariff_type_id == 1)
                                <div class="row">
                                    <div class="desc-half tm">
                                        {{ $tariff->bandwidth }} {{ $tariff->units }}
                                    </div>
                                    <div class="desc-half separator ">
                                        @if($tariff->minutes == null)
                                            Llamadas Ilimitadas
                                        @else
                                            {{ $tariff->minutes }} Minutos
                                        @endif
                                    </div>
                                </div>
                                @else
                                    <div class="fo">
                                        {{ $tariff->bandwidth }} {{ $tariff->units }}
                                    </div>
                                @endif
                            </div>
                            <div class="price">
                                {{ $tariff->price / 100 }}
                            </div>

                            <div class="iva">
                                € / mes IVA inc.
                            </div>
                         </div>
                    </a>

                  </div>
                </div>
            @endforeach

        </div>

    </div>

</div>

@endsection