@extends('layouts.frontbase')

@section('content')
<div class="album py-5 bg-light">
    <div class="container text-center">
        <h1>Acerca de Toxo</h1>

		<div class="product-back sombra p-3">
			<img src="{{ asset('img/firma.png') }}" class="img-fluid" alt="ToxoTelecom" />

			<p>
Toxo Telecom é unha operadora móbil enraizada no Concello de Ribeira, A Coruña.
Nacemos da fusión de 3 emprendedores relacionados coas telecomunicacións con ganas de revolucionar o mercado da telefonía móbil.
As nosas prioridades son básicas, cercanía, respecto e bo servizo cara xs clientxs, respecto co medio ambiente, prezos competivitos e todo isto cun toque de retranca galega.

100% Galegos 
Contacta
Podes contactar con nos a través dos seguintes medios:
602 247 247 
info@toxotelecom.com
www.toxotelecom.com

			</p>

		</div>

    </div>

</div>

@endsection