@extends('layouts.frontbase')

@section('content')
<div class="album py-5 bg-light">
    <div class="container text-center pt-5">

        <h1>Ofertas</h1>

        <div class="row ">

			@foreach($offers as $offer)
                <div class="col-md-3">
                  <div class="card mb-4 shadow-sm">
                    <a href="{{ route('oferta', ['offer' => $offer->slug]) }}" >
                        <div class="card-header">
                            {{ strtoupper ($offer->name) }}
                        </div>

                         <div class="card-body ">
                            <div class="desc">
                                @if($offer->tariff_id != null)

                                    {{ $offer->tariff->name }}
                                
                                @else

                                    {{ $offer->pack->name }}

                                @endif
                            </div>
                            <div class="price">
                                {{ $offer->price / 100 }}
                            </div>

                            <div class="iva">
                                € / mes IVA inc.
                            </div>
                         </div>
                    </a>

                  </div>
                </div>
            @endforeach
        </div>

    </div>

</div>

@endsection