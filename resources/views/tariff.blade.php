@extends('layouts.frontbase')

@section('title', $tariff->name)

@section('content')
<div class="album py-5 bg-light">
    <div class="container text-center pt-5">

        <div class="row tariff-back">
			<div class="col-md-6">
				<img src="{{ asset('img/' . $tariff->image) }}" class="img-fluid" alt="{{ $tariff->name }}" />
			</div>

			<div class="col-md-6">

				<h1>{{ $tariff->name }}</h1>

				<div class="price">
					{{ $tariff->price / 100 }} €
				</div>

				<div class="text-left m-3">
					{{ $tariff->short_description }}


					
				</div>

				<div class="text-left m-3">
					<a href="{{ route('alcarrito', ['type' => 'tarifa', 'product' => $tariff->slug]) }}" class="btn btn-info">Contratar</a>
				</div>


				<p class="description">
					{!!nl2br($tariff->description)!!}
				</p>

			</div>
        </div>
    </div>

</div>

@endsection