@extends('layouts.frontbase')

@section('title', $offer->name)

@section('content')
<div class="album py-5 bg-light">
    <div class="container text-center pt-5">

        <div class="row tariff-back">
			<div class="col-md-6">
				<img src="{{ asset('img/' . $offer->image) }}" class="img-fluid" alt="{{ $offer->name }}" />
			</div>

			<div class="col-md-6">

				<h1>{{ $offer->name }}</h1>

				<div class="price">
					{{ $offer->price / 100 }} €
				</div>

				<div>
				  Válida desde {{ $offer->date_start }} hasta {{ $offer->date_end }}
				</div>

				<div class="text-left m-3">
					{{ $offer->short_description }}


					
				</div>

				<div class="text-left m-3">
					<a href="{{ route('alcarrito', ['type' => 'oferta', 'product' => $offer->slug]) }}" class="btn btn-info">Contratar</a>
				</div>


				<p class="description">
					{!!nl2br($offer->description)!!}
				</p>

			</div>
        </div>
    </div>

</div>

@endsection