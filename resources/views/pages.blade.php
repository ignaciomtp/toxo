@extends('layouts.frontbase')

@section('title', 'Páginas')

@section('content')
<div class="album py-5 bg-light">
    <div class="container text-center">
        <h1>Pages</h1>

		@foreach($pages as $page)
			<div class="page-list">
				<a href="{{ route('custompage', ['page' => $page->slug]) }}">
					<h3>{{ $page->title }} </h3>
				</a>
			</div>
		@endforeach

    </div>

</div>

@endsection