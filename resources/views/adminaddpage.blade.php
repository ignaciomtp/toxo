   @extends('layouts.panelbase')


    @section('content')


      <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Añadir Página</h1>

      </div>


    <div class="col-md-6">
      <form class="needs-validation" action="{{ route('savepage') }}" method="post">
        @csrf

        <div class="mb-3">
          <label for="nombre">Título</label>
          <input type="text" class="form-control" name="title" id="title" required>
        </div>



        <div class="mb-3">
          <label for="price">Precio</label>
          <textarea class="form-control"  name="content" id="basic-conf" ></textarea> 
        </div>

        <button class="btn btn-primary btn-lg btn-block" type="submit">Guardar</button>
      </form>
    </div>
@endsection