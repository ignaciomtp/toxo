@extends('layouts.frontbase')

@section('content')
<div class="album py-5 bg-light">
    <div class="container text-center mb-4">

    	<div class="row text-left">
			<div class="col-8 product-back sombra p-0">
				<h2 class="m-3">PEDIDO</h2>

				<form method="post" action="{{ route('crearcliente') }}">
				@csrf
					<div class="m-3">
						<h3>Datos del Cliente</h3>

						<div class="row mb-3">
							<div class="col-3">
								<label for="foa">Tratamiento</label>
							</div>

							<div class="col-3">
								<input class="form-check-input" type="radio" name="form_of_address" id="foa1" value="Sr." checked >
								  <label class="form-check-label" for="foa1">
								    Sr.
								  </label>
							</div>
							<div class="col-5 text-left">
								<input class="form-check-input" type="radio" name="form_of_address" id="foa2" value="Sra."  >
								  <label class="form-check-label" for="foa2">
								    Sra.
								  </label>
							</div>
						</div>

						<div class="row mb-3">
							<div class="col-2">
								<label for="name">Nombre</label>
							</div>

							<div class="col-8">
								<input type="text" class="form-control @error('name') is-invalid @enderror" name="name" id="name" required >
								@error('name')
								    <div class="alert alert-danger">{{ $message }}</div>
								@enderror
							</div>

							<div class="col-2">

							</div>
						</div>

						<div class="row mb-3">
							<div class="col-2">
								<label for="surname">Apellidos</label>
							</div>

							<div class="col-8">
								<input type="text" class="form-control @error('surname') is-invalid @enderror" name="surname" id="surname" required >
								@error('surname')
								    <div class="alert alert-danger">{{ $message }}</div>
								@enderror
							</div>
							<div class="col-2">

							</div>
						</div>

						<div class="row mb-3">
							<div class="col-2">
								<label for="email">Email</label>
							</div>

							<div class="col-8">
								<input type="email" class="form-control @error('email') is-invalid @enderror" name="email" id="email" required >
								@error('email')
								    <div class="alert alert-danger">{{ $message }}</div>
								@enderror
							</div>
							<div class="col-2">

							</div>
						</div>

						<div class="row mb-3">
							<div class="col-2">
								<label for="empresa">Empresa</label>
							</div>

							<div class="col-8">
								<input type="text" class="form-control" name="company" id="empresa"  >
							</div>
							<div class="col-2">
								Opcional
							</div>
						</div>	

						<div class="row mb-3">
							<div class="col-2">
								<label for="nif">NIF</label>
							</div>

							<div class="col-8">
								<input type="text" class="form-control" name="nif" id="nif"  >
							</div>
							<div class="col-2">
								Opcional
							</div>
						</div>

						<div class="row mb-3">
							<div class="col-2">
								
							</div>

							<div class="col-8 ">
								<input class="form-check-input" type="checkbox" value="" name="offers" id="offers">
								  <label class="form-check-label" for="offers">
								    Recibir ofertas de nuestros socios
								  </label>
							</div>
							<div class="col-2">
								
							</div>
						</div>

					</div>

					<hr />

					<div class="m-3">
						<h3>Dirección</h3>
						
						<div class="row mb-3">
							<div class="col-2">
								<label for="address">Dirección</label>
							</div>

							<div class="col-5 ">
								<input type="text" class="form-control @error('address') is-invalid @enderror" name="address" id="address" required />
								@error('address')
								    <div class="alert alert-danger">{{ $message }}</div>
								@enderror
							</div>

							<div class="col-3">
								<label for="cp">Código Postal</label>
							</div>

							<div class="col-2">
								<input type="text" class="form-control @error('cp') is-invalid @enderror" name="cp" id="cp" required />
								@error('cp')
								    <div class="alert alert-danger">{{ $message }}</div>
								@enderror
							</div>
							
						</div>

						<div class="row mb-3">
							

							<div class="col-2">
								<label for="city">Localidad</label>
							</div>

							<div class="col-5">
								<input type="text" class="form-control @error('city') is-invalid @enderror" name="city" id="city" required />
								@error('city')
								    <div class="alert alert-danger">{{ $message }}</div>
								@enderror
							</div>

							<div class="col-2">
								<label for="region">Provincia</label>
							</div>

							<div class="col-3">
								<input type="text" class="form-control @error('region') is-invalid @enderror" name="region" id="region" required >
								@error('region')
								    <div class="alert alert-danger">{{ $message }}</div>
								@enderror
							</div>

						</div>
					</div>

					<hr/>

					<div class="m-3">
						<h3>Datos Bancarios</h3>
						
						<div class="row mb-3">
							<div class="col-2">
								<label for="bank">Banco</label>
							</div>

							<div class="col-8">
								<input type="text" class="form-control @error('bank') is-invalid @enderror" name="bank" id="bank" required >
								@error('bank')
								    <div class="alert alert-danger">{{ $message }}</div>
								@enderror
							</div>

							<div class="col-2">

							</div>
						</div>

						<div class="row mb-3">
							<div class="col-2">
								<label for="iban">IBAN</label>
							</div>

							<div class="col-8">
								<input type="text" class="form-control @error('iban') is-invalid @enderror" name="iban" id="iban" required >
								@error('iban')
								    <div class="alert alert-danger">{{ $message }}</div>
								@enderror
							</div>

							<div class="col-2">

							</div>
						</div>

					</div>

					<hr />

					<div class="m-3 p-3 text-center">
						<button type="submit" class="btn btn-info" >   Enviar  </button>
					</div>

				</form>

			</div>

			<div class="col-3 offset-1  p-0">
				<div class="product-back sombra pt-2 pb-3 mt-0">
					<div class="m-3">
						<div class="mt-2 mb-2">
							@if(isset($product))
								1 artículo
							@endif
						</div>
						<div class="row">
							<div class="col-8">
								Cuota mensual
							</div>
							<div class="col-4 text-right">
								@if($price != null)
									{{ $price / 100 }} €
								@else 
									0.00 €
								@endif
							</div>
						</div>
						<div class="row">
							<div class="col-8">
								Tarjeta SIM
							</div>
							<div class="col-4 text-right">
								Gratis
							</div>
						</div>


					</div>

					<hr />
						<div class="row m-2">
							<div class="col-8">
								Total (impuestos inc.)
							</div>
							<div class="col-4 text-right">
								@if($price != null)
									{{ $price / 100 }} €
								@else 
									0.00 €
								@endif
							</div>
						</div>
					
				</div>
			</div>
    	</div>
        

    </div>

</div>

@endsection