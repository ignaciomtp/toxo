<div>
	<h1>Un nuevo cliente se ha dado de alta</h1>

	<p>

    El cliente <strong>{{ $customer->name }} {{ $customer->surname }}</strong> ha contratado
    {{ $product['type'] == 'pack' ? 'el' : 'la' }} {{ $product['type'] }} <strong>{{ $product['product_name'] }}</strong>
	</p>
    
</div>