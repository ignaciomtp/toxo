@extends('layouts.frontbase')

@section('title', 'Noticias')

@section('content')
<div class="album py-5 bg-light">
    <div class="container text-center">
        <h1>Noticias</h1>

		<div class="col-md-8">
			@foreach($posts as $post)
				<div class="page-list row text-left">
					<div class="col-4">
						<img src="{{ asset('img/' . $post->image) }}" class="img-fluid" alt="{{ $post->title }}" />
					</div>

					<div class="col-8">
						<a href="{{ route('onepost', ['post' => $post->slug]) }}">
							<h3>{{ $post->title }} </h3>
						</a>
					</div>
					
				</div>
			@endforeach
		</div>

		<div class="col-md-4">

		</div>


    </div>

</div>

@endsection