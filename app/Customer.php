<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = [
    	'name',
    	'surname',
    	'form_of_address',
    	'email',
    	'password',
    	'company',
    	'nif',
    	'offers',
    	'address',
    	'city',
    	'region',
    	'cp',
    	'bank',
    	'iban',
    ];

    public function orders(){
    	return $this->hasMany('App\Order');
    }
}
