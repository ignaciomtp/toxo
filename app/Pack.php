<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pack extends Model
{
    protected $fillable = [
    	'name',
    	'slug',
        'image',
    	'mobile_tariff_id',
    	'adsl_tariff_id',
    	'fv_tariff_id',
    	'price',
    	'description',
    	'short_description',
    	'conditions',
    	'ref',
    	'valid',
    	'date_start',
    	'date_end',
    ];


    /**
     * Get the mobile tariff of this pack
     */
    public function mobileTariff(){
        return $this->hasOne('App\Tariff', 'id', 'mobile_tariff_id');
    }

    /**
     * Get the adsl tariff of this pack
     */
    public function adslTariff(){
        return $this->hasOne('App\Tariff', 'id', 'adsl_tariff_id', 'id');
    }

    public function offers(){
        return $this->hasOne('App\Offer');
    }

}
