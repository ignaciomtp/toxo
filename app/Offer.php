<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    protected $fillable = [
    	'name',
    	'slug',
    	'tariff_id',
    	'pack_id',
    	'image',
    	'price',
    	'description',
    	'short_description',
    	'conditions',
    	'ref',
    	'valid',
    	'date_start',
    	'date_end',
    ];


    public function tariff(){
    	return $this->belongsTo('App\Tariff');
    }

    public function pack(){
    	return $this->belongsTo('App\Pack');
    }

}
