<?php

namespace App\Http\Middleware;

//use Request;
use Illuminate\Http\Request;

use Closure;
use Exception;
use App\User;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;

class JwtUserMiddleware
{
    public function handle($request, Closure $next, $guard = null)
    {
        $token = $this->getToken($request);
        
        if(!$token) {
            // Unauthorized response if token not there
            return response()->json([
                'error' => 'USER_NOT_LOGED'
            ], 428);
        } 

        try {
            $credentials = JWT::decode($token, env('JWT_SECRET'), ['HS256']);
        } catch(ExpiredException $e) {
            return response()->json([
                'error' => 'TOKEN_EXPIRED'
            ], 428);
        } catch(Exception $e) {
            return response()->json([
                'error' => 'TOKEN_CORRUPTED'
            ], 400);
        } 

        $a = $credentials->email;
        $b = $credentials->sub;
        $user = User::where(
            function ($query) use ($a) {
                $query->where('email', '=', $a);
            })
            ->where(function ($query) use ($b) {
                $query->where('id', '=', $b);
            }
        )->first();
        if (!$user) { 
            return response()->json([
                'error' => 'NOT_FOUND'
            ], 404);
        } 

        $request->auth = $user;
        return $next($request);
        
    }


    public function getToken($request){
        $token = $request->get('token');
        if (!$token){
            $token = $request->header('X-ACCESS_TOKEN');
        }
        if(!$token){
            $token = $request->bearerToken();
        }
        return $token;
    }

    
}