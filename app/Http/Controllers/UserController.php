<?php

namespace App\Http\Controllers;

use Validator;

use App\User;
use App\Tariff;
use App\Type;
use App\Page;
use App\LegalText;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash; 
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use App\Mail\VerifyUser;
use Illuminate\Support\Str;
use App\Http\Controllers\AuthController;

//

class UserController extends Controller
{


    public function checkAuthorizedUser($request){
        $jwt = $request->header('X-ACCESS_TOKEN');

        return checkCredentials($jwt);
    }

    public function login(Request $request){
    	$validator = Validator::make($request->all(), [ 
            'email'    => 'required|email',
            'password' => 'required|min:6'
        ]);

        if ($validator->fails()) {
            return $this->returnKOForm( $validator->errors() ); 
        }

        $email = $request->input('email');
        $user = User::where('email', $email)->first();
        if (!$user) { 
            return $this->returnKO( ['error' => 'USER_NOT_FOUND'] ); 
        }

        //$salt = $user->salt;
        if (Hash::check($request->input('password'), $user->password)) {
            $token = app('App\Http\Controllers\AuthController')->jwt($user);
            return $this->returnOK( ['token' => $token] );   
        }

        return $this->returnKO( ['error' => 'USER_NOT_MATCH'] ); 

    }


    public function createUser(Request $request){
        $userAuthorized = $this->checkAuthorizedUser($request);

        if(!$userAuthorized || $userAuthorized == null) {
            return $this->returnKO( ['error' => 'USER_NOT_AUTHORIZED'] ); 
        } 

        $validator = Validator::make($request->all(), [ 
            'email'    => 'required|email|unique:users,email',   
            'name'     => 'required|max:255',

        ]);

        if ($validator->fails()) {
            return $this->returnKOForm( $validator->errors() ); 
        }

        $clearPassword = Str::random(8);

        $user = new User;

        $user->fill($request->all());

        $user->password = Hash::make($clearPassword);

        $user->save();

        Mail::to($user->email)->send(new VerifyUser($user, $clearPassword));

        return $this->returnOK($user);

    }

    public function users(Request $request){
        $userAuthorized = $this->checkAuthorizedUser($request);

        if(!$userAuthorized || $userAuthorized == null) {
            return $this->returnKO( ['error' => 'USER_NOT_AUTHORIZED'] ); 
        } 

        $users = User::all();

        $data = [
            'total' => count($users),
            'data' => $users
        ];

        return $this->returnOK($data);
    }

    public function getUser(Request $request, $id){
        $userAuthorized = $this->checkAuthorizedUser($request);

        if(!$userAuthorized || $userAuthorized == null) {
            return $this->returnKO( ['error' => 'USER_NOT_AUTHORIZED'] ); 
        } 

        $user = User::find($id);

        if(!$user || $user == null){
            return $this->returnKO( ['error' => 'USER_NOT_FOUND'] ); 
        }

        return $this->returnOK( $user );

    }

    public function updateUser(Request $request, $id){
        $userAuthorized = $this->checkAuthorizedUser($request);

        if(!$userAuthorized || $userAuthorized == null) {
            return $this->returnKO( ['error' => 'USER_NOT_AUTHORIZED'] ); 
        } 

        $user = User::find($id);

        if(!$user || $user == null){
            return $this->returnKO( ['error' => 'USER_NOT_FOUND'] ); 
        }

        $validator = Validator::make($request->all(), [ 
            'email'    => 'email|unique:users,email',   
            'name'     => 'max:255',

        ]);

        if ($validator->fails()) {
            return $this->returnKOForm( $validator->errors() ); 
        }

        $user->fill($request->all());

        $user->save();

        return $this->returnOK( $user );
    }


    public function destroyUser(Request $request, $id){
        $userAuthorized = $this->checkAuthorizedUser($request);

        if(!$userAuthorized || $userAuthorized == null) {
            return $this->returnKO( ['error' => 'USER_NOT_AUTHORIZED'] ); 
        } 

        $user = User::find($id);

        if(!$user || $user == null){
            return $this->returnKO( ['error' => 'USER_NOT_FOUND'] ); 
        }

        $user->delete();

        return $this->returnOK( "User $id successfuly deleted" );
    }


    public function changePassword(Request $request, $id){
        $userAuthorized = $this->checkAuthorizedUser($request);
        $userIsAuthor = true;

        if($userAuthorized) {
            $userIsAuthor = checkAuthor($request->header('X-ACCESS_TOKEN'), $id);
           
        } 

        if(!$userAuthorized || !$userIsAuthor) {
            return $this->returnKO( ['error' => 'USER_NOT_AUTHORIZED'] ); 
        }

        $validator = Validator::make($request->all(), [ 
            'password'    => 'required|confirmed|min:8', 

        ]);

        if ($validator->fails()) {
            return $this->returnKOForm( $validator->errors() ); 
        }

        $user = User::find($id);

        if(!$user || $user == null){
            return $this->returnKO( ['error' => 'USER_NOT_FOUND'] ); 
        }

        $user->password = Hash::make($request->password);

        $user->save();

        return $this->returnOK( $user );

    }

}
