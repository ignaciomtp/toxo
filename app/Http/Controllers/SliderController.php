<?php

namespace App\Http\Controllers;

use Validator;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

use App\Slide;

class SliderController extends Controller
{
	public function checkAuthorizedUser($request){
        $jwt = $request->header('X-ACCESS_TOKEN');

        return checkCredentials($jwt);
    }
    
	public function slides(){

		$slides = Slide::all();

		$data = [
    		'total' => count($slides),
    		'data' => $slides
    	];

    	return $this->returnOK( $data );

	}

	public function getSlide($id){
		$slide = Slide::find($id);

    	if(!$slide || $slide == null){
        	return $this->returnKO( ['error' => 'SLIDE_NOT_FOUND'] ); 
        }

    	return $this->returnOK( $slide );
	}

	public function createSlide(Request $request){
		$userAuthorized = $this->checkAuthorizedUser($request);

        if(!$userAuthorized || $userAuthorized == null) {
            return $this->returnKO( ['error' => 'USER_NOT_AUTHORIZED'] ); 
        } 

        $validator = Validator::make($request->all(), [ 
            'title' => 'required|max:50',
            'link' => 'required|max:100',
            'image' => 'image|required|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($validator->fails()) {
            return $this->returnKOForm( $validator->errors() ); 
        }

        $slide = new Slide;

        $slide->fill($request->all());

        $image = $request->file('image');
        $nameImage = time().$image->getClientOriginalName();
        $image->move(public_path('img'), $nameImage);

        $slide->image = $nameImage;

        $slide->save();

        return $this->returnOK( $slide );

	}

	public function updateSlide(Request $request, $id){
		$userAuthorized = $this->checkAuthorizedUser($request);

        if(!$userAuthorized || $userAuthorized == null) {
            return $this->returnKO( ['error' => 'USER_NOT_AUTHORIZED'] ); 
        } 

        $validator = Validator::make($request->all(), [ 
            'title' => 'max:50',
            'link' => 'max:100',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($validator->fails()) {
            return $this->returnKOForm( $validator->errors() ); 
        }

        $slide = Slide::find($id);

    	if(!$slide || $slide == null){
        	return $this->returnKO( ['error' => 'SLIDE_NOT_FOUND'] ); 
        }

        $img = $slide->image;
        $slide->fill($request->all());

        if($request->hasFile('image')) {
        	$oldImage = public_path('img/').$img;
        	error_log('OldImage: ' . $oldImage);
            File::delete($oldImage);

            $image = $request->file('image');
            $nameImage = time().$image->getClientOriginalName();
            $image->move(public_path('img'), $nameImage);

            $slide->image = $nameImage;
          
        } 

        $slide->save();

        return $this->returnOK( $slide );

	}


	public function destroySlide(Request $request, $id){
		$userAuthorized = $this->checkAuthorizedUser($request);

        if(!$userAuthorized || $userAuthorized == null) {
            return $this->returnKO( ['error' => 'USER_NOT_AUTHORIZED'] ); 
        } 


        $slide = Slide::find($id);

        if(!$slide || $slide == null){
        	return $this->returnKO( ['error' => 'SLIDE_NOT_FOUND'] ); 
        }

        if($slide->image != null){
        	File::delete(public_path('img/').$slide->image);
        }

        $slide->delete();

        return $this->returnOK( "Slide $id successfuly deleted" );
	}

}
