<?php

namespace App\Http\Controllers;

use Validator;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Http\Controllers\AuthController;

use App\Page;

class PageController extends Controller
{
    public function checkAuthorizedUser($request){
        $jwt = $request->header('X-ACCESS_TOKEN');

        return checkCredentials($jwt);
    }


	public function pages(){
		$pages = Page::all();

    	$data = [
    		'total' => count($pages),
    		'data' => $pages
    	];

    	return $this->returnOK( $data );
	}

	public function getPage($id){
    	$page = Page::find($id);

    	if(!$page || $page == null){
        	return $this->returnKO( ['error' => 'PAGE_NOT_FOUND'] ); 
        }

    	return $this->returnOK( $page );
    }

    public function createPage(Request $request){
    	$userAuthorized = $this->checkAuthorizedUser($request);

        if(!$userAuthorized || $userAuthorized == null) {
            return $this->returnKO( ['error' => 'USER_NOT_AUTHORIZED'] ); 
        } 

        $validator = Validator::make($request->all(), [ 
            'title' => 'required|max:100',
            'body' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->returnKOForm( $validator->errors() ); 
        }


        $page = new Page;

        $page->title = $request->title;
        $page->slug = Str::slug($request->title, '-'); 
        $page->body = $request->body;

        $page->save();

        return $this->returnOK($page);

    }

    public function updatePage(Request $request, $id){
    	$userAuthorized = $this->checkAuthorizedUser($request);

        if(!$userAuthorized || $userAuthorized == null) {
            return $this->returnKO( ['error' => 'USER_NOT_AUTHORIZED'] ); 
        } 

        $validator = Validator::make($request->all(), [ 
            'title' => 'max:100',
            
        ]);

        if ($validator->fails()) {
            return $this->returnKOForm( $validator->errors() ); 
        }


        $page = Page::find($id);

        if(!$page || $page == null){
        	return $this->returnKO( ['error' => 'PAGE_NOT_FOUND'] ); 
        }

        $page->fill($request->all());

        if($request->has('title')){
        	$page->slug = Str::slug($request->title, '-'); 
        }

        $page->save();

        return $this->returnOK( $page );   	

    }

    public function destroyPage(Request $request, $id){

        $userAuthorized = $this->checkAuthorizedUser($request);

        if(!$userAuthorized || $userAuthorized == null) {
            return $this->returnKO( ['error' => 'USER_NOT_AUTHORIZED'] ); 
        } 


        $page = Page::find($id);

        if(!$page || $page == null) {
            return $this->returnKO( ['error' => 'Page_NOT_FOUND'] ); 
        }

        $page->delete();

        return $this->returnOK( "Page $id successfuly deleted" );

    }

}
