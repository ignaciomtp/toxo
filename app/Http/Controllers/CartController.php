<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CartController extends Controller
{
    
	public function __construct(){
		if(!\Session::has('carrito')) \Session::put('carrito', array());
	}


	public function addToCart($type, $product){
		//$carrito = \Session::get('carrito');

		$carrito = [];

		$item = [
			'type' => $type,
			'product' => $product,
		];

		array_push($carrito, $item);

		\Session::put('carrito', $carrito);

		return redirect()->route('carrito');

	}

	public function removeFromCart(){
		\Session::put('carrito', []);

		return redirect()->route('carrito');
	}


}
