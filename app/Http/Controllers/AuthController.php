<?php

namespace App\Http\Controllers;

use Validator;
use App\Customer;
use App\User;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Firebase\JWT\ExpiredException;
use Illuminate\Support\Facades\Hash; 

class AuthController extends Controller { 

    public static function jwt(User $user) {
        $payload = [
            'email' => $user->email,
            'name' => $user->name,
            'sub' => $user->id, // Subject of the token
            'iat' => time(), // Time when JWT was issued. 
            'exp' => time() + 7*60*60*24 // Expiration time
        ];
         
        return JWT::encode($payload, env('JWT_SECRET'));
    }

    public static function jwtCustomer(Customer $user) {
        $payload = [
            'email' => $user->email,
            'name' => $user->name,
            'sub' => $user->id, // Subject of the token
            'iat' => time(), // Time when JWT was issued. 
            'exp' => time() + 7*60*60*24 // Expiration time
        ];
         
        return JWT::encode($payload, env('JWT_SECRET'));
    }

     
}