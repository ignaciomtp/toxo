<?php

namespace App\Http\Controllers;

use Validator;

use App\Tariff;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash; 
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use App\Http\Controllers\AuthController;

class TariffsController extends Controller
{

    public function checkAuthorizedUser($request){
        $jwt = $request->header('X-ACCESS_TOKEN');

        return checkCredentials($jwt);
    }

    public function tariffs(){
    	$tariffs = Tariff::all();

    	$data = [
    		'total' => count($tariffs),
    		'data' => $tariffs
    	];

    	return $this->returnOK( $data );
    }

    public function getTariff($id){
        $tariff = Tariff::find($id);

        if(!$tariff || $tariff == null) {
            return $this->returnKO( ['error' => 'TARIFF_NOT_FOUND'] ); 
        }

        return $this->returnOK( $tariff );
    }


    public function createTariff(Request $request){

        $userAuthorized = $this->checkAuthorizedUser($request);

        if(!$userAuthorized || $userAuthorized == null) {
            return $this->returnKO( ['error' => 'USER_NOT_AUTHORIZED'] ); 
        } 


        $validator = Validator::make($request->all(), [ 
            'name' => 'required|max:30',
            'description' => 'required',
            'short_description' => 'required|max:255',
            'price' => 'required',
            'ref' => 'required|max:20',
            'tariff_type_id' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($validator->fails()) {
            return $this->returnKOForm( $validator->errors() ); 
        }

        $imageName = time().$request->image->getClientOriginalName();  
   
        $request->image->move(public_path('img'), $imageName);

        $tariff = new Tariff;

        $tariff->fill($request->all());

        $tariff->slug = Str::slug($request->name, '-'); 
        $tariff->image = $imageName;

        $tariff->save();

        return $this->returnOK( $tariff );
    }

    public function updateTariff(Request $request, $id){

        $userAuthorized = $this->checkAuthorizedUser($request);

        if(!$userAuthorized || $userAuthorized == null) {
            return $this->returnKO( ['error' => 'USER_NOT_AUTHORIZED'] ); 
        } 

        $tariff = Tariff::find($id);

        if(!$tariff || $tariff == null) {
            return $this->returnKO( ['error' => 'TARIFF_NOT_FOUND'] ); 
        }

        $img = $tariff->image;
        $tariff->fill($request->all());

        if($request->has('name')){
            $tariff->slug = Str::slug($request->name, '-'); 
        }

        if($request->hasFile('image')) {
            $oldImage = public_path('img/').$img;
            File::delete($oldImage);

            $image = $request->file('image');
            $nameImage = time().$image->getClientOriginalName();
            $image->move(public_path('img'), $nameImage);
          
            $tariff->image = $nameImage;
        } 

        $tariff->save();

        return $this->returnOK( $tariff );

    }

    public function destroyTariff(Request $request, $id){

        $userAuthorized = $this->checkAuthorizedUser($request);

        if(!$userAuthorized || $userAuthorized == null) {
            return $this->returnKO( ['error' => 'USER_NOT_AUTHORIZED'] ); 
        } 


        $tariff = Tariff::find($id);

        if(!$tariff || $tariff == null) {
            return $this->returnKO( ['error' => 'TARIFF_NOT_FOUND'] ); 
        }

        if($tariff->image != null){
            File::delete(public_path('img/').$tariff->image);
        }

        $tariff->delete();

        return $this->returnOK( "Tariff $id successfuly deleted" );

    }


}
