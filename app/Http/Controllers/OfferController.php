<?php

namespace App\Http\Controllers;

use Validator;

use App\Offer;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash; 
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use App\Http\Controllers\AuthController;

class OfferController extends Controller
{
    public function checkAuthorizedUser($request){
        $jwt = $request->header('X-ACCESS_TOKEN');

        return checkCredentials($jwt);
    }


    public function offers(){
    	$offers = Offer::all();

    	$data = [
    		'total' => count($offers),
    		'data' => $offers
    	];

    	return $this->returnOK( $data );
    }


    public function getOffer($id){

    	$offer = Offer::find($id);

    	if(!$offer || $offer == null) {
            return $this->returnKO( ['error' => 'OFFER_NOT_FOUND'] ); 
        }

        return $this->returnOK( $offer );

    }


    public function createOffer(Request $request){
		$userAuthorized = $this->checkAuthorizedUser($request);

        if(!$userAuthorized || $userAuthorized == null) {
            return $this->returnKO( ['error' => 'USER_NOT_AUTHORIZED'] ); 
        } 


        $validator = Validator::make($request->all(), [ 
            'name' => 'required|max:30',
            'description' => 'required',
            'short_description' => 'required|max:255',
            'price' => 'required',
            'ref' => 'required|max:20',
            'conditions' => 'required|max:150',
            'image' => '|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'date_start' => 'required|date',
            'date_end' => 'required|date',
        ]);

        if ($validator->fails()) {
            return $this->returnKOForm( $validator->errors() ); 
        }

        if($request->hasFile('image')) {
            $image = $request->file('image');
            $nameImage = time().$image->getClientOriginalName();
            $image->move(public_path('img'), $nameImage);
          
        } 

        $offer = new Offer;

        $offer->fill($request->all());

        $offer->slug = Str::slug($request->name, '-'); 
        $offer->image = $nameImage;

        $offer->save();

        return $this->returnOK( $offer );

    }


    public function updateOffer(Request $request, $id){
    	$userAuthorized = $this->checkAuthorizedUser($request);

        if(!$userAuthorized || $userAuthorized == null) {
            return $this->returnKO( ['error' => 'USER_NOT_AUTHORIZED'] ); 
        } 

        $offer = Offer::find($id);

        if(!$offer || $offer == null) {
            return $this->returnKO( ['error' => 'OFFER_NOT_FOUND'] ); 
        }

        $img = $offer->image;
        $offer->fill($request->all());

        if($request->hasFile('image')) {
            $oldImage = public_path('img/').$img;
            File::delete($oldImage);

            $image = $request->file('image');
            $nameImage = time().$image->getClientOriginalName();
            $image->move(public_path('img'), $nameImage);

            $offer->image = $nameImage;          
        } 

        if($request->has('name')){
            $offer->slug = Str::slug($request->name, '-'); 
        }

        $offer->save();

        return $this->returnOK( $offer );

    }


    public function destroyOffer(Request $request, $id){
    	$userAuthorized = $this->checkAuthorizedUser($request);

        if(!$userAuthorized || $userAuthorized == null) {
            return $this->returnKO( ['error' => 'USER_NOT_AUTHORIZED'] ); 
        } 


        $offer = Offer::find($id);

        if(!$offer || $offer == null) {
            return $this->returnKO( ['error' => 'OFFER_NOT_FOUND'] ); 
        }

        if($offer->image != null){
            File::delete(public_path('img/').$offer->image);
        }

        $offer->delete();

        return $this->returnOK( "Offer $id successfuly deleted" );
    }


}
