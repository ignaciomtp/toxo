<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;
use Illuminate\Support\Str;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function pages(){

        $pages = Page::all();

        return view('adminpages', compact('pages'));

    }

    public function addPage(){
        return view('adminaddpage');
    }

    public function savePage(Request $request){

        $page = new Page;

        $slug = Str::slug($request->title, '-');

        $page->title = $request->title;
        $page->body = $request->content;
        $page->slug = $slug;


        $page->save();

        return redirect()->route('pages');

    }


}
