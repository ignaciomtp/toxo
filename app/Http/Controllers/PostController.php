<?php

namespace App\Http\Controllers;

use Validator;

use App\Post;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash; 
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use App\Http\Controllers\AuthController;

class PostController extends Controller
{
    public function checkAuthorizedUser($request){
        $jwt = $request->header('X-ACCESS_TOKEN');

        return checkCredentials($jwt);
    }

    public function posts(){
    	$posts = Post::all();

    	$data = [
    		'total' => count($posts),
    		'data' => $posts
    	];

    	return $this->returnOK( $data );
    }


    public function getPost($id){
    	$post = Post::find($id);

    	if(!$post || $post == null){
        	return $this->returnKO( ['error' => 'POST_NOT_FOUND'] ); 
        }

    	return $this->returnOK( $post );
    }


    public function createPost(Request $request){
    	$userAuthorized = $this->checkAuthorizedUser($request);

        if(!$userAuthorized || $userAuthorized == null) {
            return $this->returnKO( ['error' => 'USER_NOT_AUTHORIZED'] ); 
        } 

        $validator = Validator::make($request->all(), [ 
            'title' => 'required|max:100',
            'body' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($validator->fails()) {
            return $this->returnKOForm( $validator->errors() ); 
        }


        $post = new Post;

        $post->fill($request->all());

        $post->slug = Str::slug($request->title, '-'); 

        if($request->hasFile('image')) {
            $image = $request->file('image');
            $nameImage = time().'.'.$image->getClientOriginalName();
            $image->move(public_path('img'), $nameImage);
          
          	$post->image = $nameImage;
        } 

        $post->date = date("Y-m-d");

        $post->save();

        return $this->returnOK( $post );

    }


    public function updatePost(Request $request, $id){
    	$userAuthorized = $this->checkAuthorizedUser($request);

        if(!$userAuthorized || $userAuthorized == null) {
            return $this->returnKO( ['error' => 'USER_NOT_AUTHORIZED'] ); 
        } 

        $validator = Validator::make($request->all(), [ 
            'title' => 'max:100',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($validator->fails()) {
            return $this->returnKOForm( $validator->errors() ); 
        }


        $post = Post::find($id);

        if(!$post || $post == null){
        	return $this->returnKO( ['error' => 'POST_NOT_FOUND'] ); 
        }

        $img = $post->image;
        $post->fill($request->all());

        if($request->has('name')){
        	$post->slug = Str::slug($request->title, '-'); 
        }

        if($request->hasFile('image')) {
        	$oldImage = public_path('img/').$img;
            File::delete($oldImage);

            $image = $request->file('image');
            $nameImage = time().'.'.$image->getClientOriginalName();
            $image->move(public_path('img'), $nameImage);
          
          	$post->image = $nameImage;
        } 

        $post->save();

        return $this->returnOK( $post );

    }


    public function destroyPost(Request $request, $id){

        $userAuthorized = $this->checkAuthorizedUser($request);

        if(!$userAuthorized || $userAuthorized == null) {
            return $this->returnKO( ['error' => 'USER_NOT_AUTHORIZED'] ); 
        } 


        $post = Post::find($id);

        if(!$post || $post == null) {
            return $this->returnKO( ['error' => 'POST_NOT_FOUND'] ); 
        }

        if($post->image != null){
        	File::delete(public_path('img/').$post->image);
        }

        $post->delete();

        return $this->returnOK( "Post $id successfuly deleted" );

    }

}
