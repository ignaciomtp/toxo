<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function returnOK($data, $code=200){
    	return response()->json($data, $code);
    }

    public function returnKO($data, $code=400){
    	return response()->json($data, $code);
    }

    public function returnKOForm($validator, $code=401){
    	$err = array();
		$err['error'] = 'FORM_VALIDATION';
		$err['validation'] = $validator;
		return $this->returnKO( $err, $code ); 
    }
}
