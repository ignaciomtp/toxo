<?php

namespace App\Http\Controllers;

use Validator;

use App\LegalText;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash; 
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use App\Http\Controllers\AuthController;

class LegalTextsController extends Controller
{

    public function checkAuthorizedUser($request){
        $jwt = $request->header('X-ACCESS_TOKEN');

        return checkCredentials($jwt);
    }

    public function legalTexts(){

        $legalTexts = LegalText::all();

        return $this->returnOK($legalTexts);
    }

    public function getLegalText($id){

        $text = LegalText::find($id);

        if(!$text || $text == null) {
            return $this->returnKO( ['error' => 'TEXT_NOT_FOUND'] ); 
        }

        return $this->returnOK($text);
    }


    public function createText(Request $request){

        $userAuthorized = $this->checkAuthorizedUser($request);

        if(!$userAuthorized || $userAuthorized == null) {
            return $this->returnKO( ['error' => 'USER_NOT_AUTHORIZED'] ); 
        } 


        $validator = Validator::make($request->all(), [ 
            'title' => 'required|max:100',
            'body' => 'required',
            
        ]);

        if ($validator->fails()) {
            return $this->returnKOForm( $validator->errors() ); 
        }

        $text = new LegalText;

        $text->title = $request->title;
        $text->slug = Str::slug($request->title, '-');
        $text->body = $request->body;

        $text->save();

        return $this->returnOK($text);

    }

    public function updateText(Request $request, $id){

        $userAuthorized = $this->checkAuthorizedUser($request);

        if(!$userAuthorized || $userAuthorized == null) {
            return $this->returnKO( ['error' => 'USER_NOT_AUTHORIZED'] ); 
        } 

        $validator = Validator::make($request->all(), [ 
            'title' => '|max:100',
            
        ]);

        if ($validator->fails()) {
            return $this->returnKOForm( $validator->errors() ); 
        }

        $text = LegalText::find($id);

        if(!$text || $text == null) {
            return $this->returnKO( ['error' => 'TEXT_NOT_FOUND'] ); 
        }

        $text->fill($request->all());

        if($request->has('title')){
            $text->slug = Str::slug($request->title, '-'); 
        }

        $text->save();

        return $this->returnOK($text);

    }

    public function destroyText(Request $request, $id){
        $userAuthorized = $this->checkAuthorizedUser($request);

        if(!$userAuthorized || $userAuthorized == null) {
            return $this->returnKO( ['error' => 'USER_NOT_AUTHORIZED'] ); 
        } 

        $text = LegalText::find($id);

        if(!$text || $text == null) {
            return $this->returnKO( ['error' => 'TEXT_NOT_FOUND'] ); 
        }

        $text->delete();

        return $this->returnOK( "Text $id successfuly deleted" );

    }

}
