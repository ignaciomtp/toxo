<?php

namespace App\Http\Controllers;

use Validator;
use DB;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Mail\WelcomeMail;
use App\Mail\AlertNewCustomer;
use Illuminate\Support\Facades\Mail;
use App\Customer;
use App\User;
use App\Order;

class CustomerController extends Controller
{

	public function checkAuthorizedUser($request){
        $jwt = $request->header('X-ACCESS_TOKEN');

        return checkCredentials($jwt);
    }
    
	public function createCustomer(Request $request){

        $this->validate($request, [
            'name' => 'required|max:30',
            'surname' => 'required|max:100',
            'email' => 'required|email|unique:customers,email',            
            'address' => 'required|max:100',
            'city' => 'required|max:50',
            'region' => 'required|max:50',
            'cp' => 'required|max:5',
            'bank' => 'required|max:50',
            'iban' => 'required|max:24',
        ]);

        $customer = new Customer;

        $customer->fill($request->all());

        if($request->has('offers')) $customer->offers = true;

        $clearPassword = Str::random(8);

        $customer->password = Hash::make($clearPassword);

        $customer->save();

        Mail::to($customer->email)->send(new WelcomeMail($customer, $clearPassword));

        $cart = session('carrito');

        $order = new Order;

        $order->customer_id = $customer->id;
        $order->product_type = $cart[0]['type'];
        $order->product_id = $cart[0]['product_id'];
        $order->order_type = 'Alta nueva';
        $order->shipping_method_id = 1;

        $product = $cart[0];

        $order->save();

        \Session::put('carrito', []);

        $users = User::all();

        foreach($users as $user){
        	//Mail::to($user->email)->send(new AlertNewCustomer($customer, $product));
        	Mail::to('ignacio.martinez@ctbarbanza.com')->send(new AlertNewCustomer($customer, $product));
        }

        return redirect()->route('inicio');

	}

	public function customers(){

		$customers = DB::table('customers')->paginate(15);

		$count = Customer::count();   

		$data = [
    		'total' => $count,
    		'data' => $customers,
    	];

    	return $this->returnOK( $data );

	}

	public function getCustomer($id){

		$customer = Customer::find($id);

		if(!$customer || $customer == null){
        	return $this->returnKO( ['error' => 'CUSTOMER_NOT_FOUND'] ); 
        }

    	return $this->returnOK( $customer );
	}

	public function updateCustomer(Request $request, $id){
		$userAuthorized = $this->checkAuthorizedUser($request);
		$userIsAuthor = true;

        if($userAuthorized) {
        	$userIsAuthor = checkAuthor($request->header('X-ACCESS_TOKEN'), $id);
           
        } 

        if(!$userAuthorized || !$userIsAuthor) {
        	return $this->returnKO( ['error' => 'USER_NOT_AUTHORIZED'] ); 
        }

        $customer = Customer::find($id);

		if(!$customer || $customer == null){
        	return $this->returnKO( ['error' => 'CUSTOMER_NOT_FOUND'] ); 
        }

        $customer->fill($request->all());

        $customer->save();

        return $this->returnOK( $customer );

	}


    public function destroyCustomer(Request $request, $id){
        $userAuthorized = $this->checkAuthorizedUser($request);

        if(!$userAuthorized || $userAuthorized == null) {
            return $this->returnKO( ['error' => 'USER_NOT_AUTHORIZED'] ); 
        } 


        $customer = Customer::find($id);

        if(!$customer || $customer == null) {
            return $this->returnKO( ['error' => 'CUSTOMER_NOT_FOUND'] ); 
        }

        $customer->delete();

        return $this->returnOK( "Customer $id successfuly deleted" );
    }


}
