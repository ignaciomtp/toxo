<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tariff;
use App\Type;
use App\Page;
use App\Pack;
use App\Post;
use App\Offer;
use App\LegalText;
use App\Slide;

class PublicController extends Controller
{

    protected $legalSlugs;

    protected $customPages;

    public function __construct(){
        $this->legalSlugs = getLegalTextSlugs();
        $this->customPages = getCustomPages();
    }

    public function index()
    {
        
        $legalSlugs = $this->legalSlugs;
        $customPages = $this->customPages;    

        $cart = session('carrito');

        $cartItems = $cart ? count($cart) : 0;

        $slides = Slide::all();

    	$tariffs = Tariff::where('valid', true)->orderBy('id', 'desc')->take(8)->get();

        return view('inicio', compact('tariffs', 'legalSlugs', 'cartItems', 'customPages', 'slides'));
    }


    public function about(){

        $legalSlugs = $this->legalSlugs;
        $customPages = $this->customPages;    

        $cart = session('carrito');

        $cartItems = $cart ? count($cart) : 0;

    	return view('acerca', compact('cartItems', 'legalSlugs', 'customPages'));
    }

    public function contact(){
        $legalSlugs = $this->legalSlugs;
        $customPages = $this->customPages;    

        $cart = session('carrito');

        $cartItems = $cart ? count($cart) : 0;

    	return view('contacto', compact('cartItems', 'customPages'));
    }

    public function TariffsByType($type){
        $legalSlugs = $this->legalSlugs;
        $customPages = $this->customPages;    

        $cart = session('carrito');

        $cartItems = $cart ? count($cart) : 0;

    	$typeTariff = Type::where('slug', $type)->first();

    	if(!$typeTariff || $typeTariff == null){
    		return view('error');
    	}

    	$tariffs = Tariff::where('tariff_type_id', $typeTariff->id)->get();

    	return view('category', compact('tariffs', 'typeTariff', 'legalSlugs', 'cartItems', 'customPages'));

    }


    public function seeTariff($type, $tariff){
        $legalSlugs = $this->legalSlugs;
        $customPages = $this->customPages;    

        $cart = session('carrito');

        $cartItems = $cart ? count($cart) : 0;

    	$typeTariff = Type::where('slug', $type)->first();

    	if(!$typeTariff || $typeTariff == null){
    		return view('error');
    	}

    	$tariff = Tariff::where('slug', $tariff)->first();

    	if(!$tariff || $tariff == null){
    		return view('error');
    	}

    	return view('tariff', compact('tariff', 'typeTariff', 'legalSlugs', 'cartItems', 'customPages'));

    }


    public function pages(){
        $legalSlugs = $this->legalSlugs;
        $customPages = $this->customPages;    

        $cart = session('carrito');

        $cartItems = $cart ? count($cart) : 0;

    	$pages = Page::all();

    	return view('pages', compact('pages', 'cartItems', 'legalSlugs', 'customPages'));
    }

    public function seePage($page){
        $legalSlugs = $this->legalSlugs;
        $customPages = $this->customPages;    

        $cart = session('carrito');

        $cartItems = $cart ? count($cart) : 0;

    	$page = Page::where('slug', $page)->first();

    	if(!$page || $page == null){
    		return view('error');
    	}

    	return view('custompage', compact('page', 'legalSlugs', 'cartItems', 'customPages'));
    }


    public function getLegalText($text){
        $legalSlugs = $this->legalSlugs;
        $customPages = $this->customPages;    

        $cart = session('carrito');

        $cartItems = $cart ? count($cart) : 0;

        $legalText = LegalText::where('slug', $text)->first();

        if(!$legalText || $legalText == null){
            return view('error');
        }

        return view('legal', compact('legalText', 'legalSlugs', 'cartItems', 'customPages'));

    }


    public function packs(){
        $legalSlugs = $this->legalSlugs;
        $customPages = $this->customPages;    

        $cart = session('carrito');

        $cartItems = $cart ? count($cart) : 0;

        $packs = Pack::where('valid', true)->get()->sortByDesc('id');;

        return view('packs', compact('packs', 'legalSlugs', 'cartItems', 'customPages'));
    }


    public function seePack($pack){
        $legalSlugs = $this->legalSlugs;
        $customPages = $this->customPages;    

        $cart = session('carrito');

        $cartItems = $cart ? count($cart) : 0;

        $pack = Pack::where('slug', $pack)->first();

        if(!$pack || $pack == null){
            return view('error');
        }

        return view('pack', compact('pack', 'legalSlugs', 'cartItems', 'customPages'));
    }


    public function allPosts(){
        $legalSlugs = $this->legalSlugs;
        $customPages = $this->customPages;    

        $cart = session('carrito');

        $cartItems = $cart ? count($cart) : 0;

        $posts = Post::all();

        return view('news', compact('posts', 'legalSlugs', 'cartItems', 'customPages'));
    }


    public function getPost($post){
        $legalSlugs = $this->legalSlugs;
        $customPages = $this->customPages;    

        $cart = session('carrito');

        $cartItems = $cart ? count($cart) : 0;

        $post = Post::where('slug', $post)->first();

        if(!$post || $post == null){
            return view('error');
        }

        return view('post', compact('post', 'legalSlugs', 'cartItems', 'customPages'));

    }


    public function offers(){
        $legalSlugs = $this->legalSlugs;
        $customPages = $this->customPages;    

        $cart = session('carrito');

        $cartItems = $cart ? count($cart) : 0;

        $offers = Offer::all();

        return view('offers', compact('offers', 'legalSlugs', 'cartItems', 'customPages'));

    }

    public function seeOffer($offer){
        $legalSlugs = $this->legalSlugs;
        $customPages = $this->customPages;    

        $cart = session('carrito');

        $cartItems = $cart ? count($cart) : 0;

        $offer = Offer::where('slug', $offer)->first();

        if(!$offer || $offer == null){
            return view('error');
        }

        return view('offer', compact('offer', 'legalSlugs', 'cartItems', 'customPages'));


    }

    public function cart(){
        $legalSlugs = $this->legalSlugs;
        $customPages = $this->customPages;    

        $cart = session('carrito');

        $cartItems = $cart ? count($cart) : 0;

        if($cart){
            $type = $cart[0]['type'];

            switch ($type) {
                case 'tarifa':
                    $product = Tariff::where('slug', $cart[0]['product'])->first();
                    break;
                
                case 'oferta':
                    $product = Offer::where('slug', $cart[0]['product'])->first();
                    break;

                case 'pack':
                    $product = Pack::where('slug', $cart[0]['product'])->first();
                    break;

                default:
                    $product = null;
                    break;
            }

            if(!$product || $product == null){
                return view('error');
            }

            $cart[0]['price'] = $product->price;
            $cart[0]['product_id'] = $product->id;
            $cart[0]['product_name'] = $product->name;

            \Session::put('carrito', $cart);

            return view('carrito', compact('legalSlugs', 'cartItems', 'product', 'customPages'));

        }

        return view('carrito', compact('legalSlugs', 'cartItems'));
    }


    public function addCustomer(){
        $legalSlugs = $this->legalSlugs;
        $customPages = $this->customPages;    

        $cart = session('carrito');

        $cartItems = $cart ? count($cart) : 0;

        $price = $cart ? $cart[0]['price'] : null;

        return view('alta', compact('legalSlugs', 'cartItems', 'price', 'customPages'));
    }

}
