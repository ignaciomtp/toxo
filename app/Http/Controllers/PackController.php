<?php

namespace App\Http\Controllers;

use Validator;

use App\Pack;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash; 
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use App\Http\Controllers\AuthController;

class PackController extends Controller
{
    public function checkAuthorizedUser($request){
        $jwt = $request->header('X-ACCESS_TOKEN');

        return checkCredentials($jwt);
    }

    public function packs(){
    	$packs = Pack::all();

    	$data = [
    		'total' => count($packs),
    		'data' => $packs
    	];

    	return $this->returnOK( $data );
    }

    public function getPack($id){
    	$pack = Pack::find($id);

    	if(!$pack || $pack == null){
        	return $this->returnKO( ['error' => 'PACK_NOT_FOUND'] ); 
        }

    	return $this->returnOK( $pack );
    }

    public function createPack(Request $request){
    	$userAuthorized = $this->checkAuthorizedUser($request);

        if(!$userAuthorized || $userAuthorized == null) {
            return $this->returnKO( ['error' => 'USER_NOT_AUTHORIZED'] ); 
        } 

        $validator = Validator::make($request->all(), [ 
            'name' => 'required|max:30',
            'description' => 'required',
            'short_description' => 'required|max:255',
            'price' => 'required',
            'ref' => 'required|max:20',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($validator->fails()) {
            return $this->returnKOForm( $validator->errors() ); 
        }


        $pack = new Pack;

        $pack->fill($request->all());

        $pack->slug = Str::slug($request->name, '-'); 

        if($request->hasFile('image')) {
            $image = $request->file('image');
            $nameImage = time().$image->getClientOriginalName();
            $image->move(public_path('img'), $nameImage);
          
          	$pack->image = $nameImage;
        } 

        $pack->save();

        return $this->returnOK( $pack );

    }

    public function updatePack(Request $request, $id){
    	$userAuthorized = $this->checkAuthorizedUser($request);

        if(!$userAuthorized || $userAuthorized == null) {
            return $this->returnKO( ['error' => 'USER_NOT_AUTHORIZED'] ); 
        } 

        $validator = Validator::make($request->all(), [ 
            'name' => 'max:30',
            'short_description' => 'max:255',
            'ref' => 'max:20',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($validator->fails()) {
            return $this->returnKOForm( $validator->errors() ); 
        }


        $pack = Pack::find($id);

        if(!$pack || $pack == null){
        	return $this->returnKO( ['error' => 'PACK_NOT_FOUND'] ); 
        }

        $img = $pack->image;
        $pack->fill($request->all());

        if($request->has('name')){
        	$pack->slug = Str::slug($request->name, '-'); 
        }

        
        if($request->hasFile('image')) {
        	$oldImage = public_path('img/').$img;
            File::delete($oldImage);

            $image = $request->file('image');
            $nameImage = time().$image->getClientOriginalName();
            $image->move(public_path('img'), $nameImage);

            $pack->image = $nameImage;
          
        } 

        $pack->save();

        return $this->returnOK( $pack );   	
    }


    public function destroyPack(Request $request, $id){

        $userAuthorized = $this->checkAuthorizedUser($request);

        if(!$userAuthorized || $userAuthorized == null) {
            return $this->returnKO( ['error' => 'USER_NOT_AUTHORIZED'] ); 
        } 


        $pack = Pack::find($id);

        if(!$pack || $pack == null) {
            return $this->returnKO( ['error' => 'PACK_NOT_FOUND'] ); 
        }

        if($pack->image != null){
        	File::delete(public_path('img/').$pack->image);
        }

        $pack->delete();

        return $this->returnOK( "Pack $id successfuly deleted" );

    }

}
