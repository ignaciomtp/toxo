<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tariff extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        'description',
        'short_description',
        'conditions',
        'valid',
        'date_start',
        'date_end',
        'bandwidth',
        'units',
        'price',
        'minutes',
        'ref',
        'unlimited_calls',
        'tariff_type_id',
        'visible'
    ];

    
    /**
     * Get the type of this tariff
     */
    public function type(){
        return $this->belongsTo('App\Type', 'tariff_type_id', 'id');
    }


    public function pack(){
        return $this->belongsTo('App\Pack');
    }

    public function offers(){
        return $this->haOne('App\Offer');
    }

}
