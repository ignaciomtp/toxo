<?php

namespace App\Mail;

use App\Customer;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AlertNewCustomer extends Mailable
{
    use Queueable, SerializesModels;

    public $customer;

    public $product;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Customer $customer, $product)
    {
        $this->customer = $customer;
        $this->product = $product;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('soporte@toxotelecom.com')
                    ->view('emails.alert');
    }
}
