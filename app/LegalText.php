<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LegalText extends Model
{
    protected $table = "legal_texts";

    protected $fillable = [
    	'title',
    	'slug',
    	'body',
    ];
}
