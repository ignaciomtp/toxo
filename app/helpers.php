<?php

use App\LegalText;
use App\Page;
use App\User;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;

if (! function_exists('getLegalTextSlugs')) {
    function getLegalTextSlugs(){
        $slugs = LegalText::all()->pluck('title', 'slug');

        $array = json_decode(json_encode($slugs), true);

        $legalSlugs = [];

        foreach($array as $clave => $valor){
            array_push($legalSlugs, (object)[
                'slug' => $clave,
                'title' => $valor
            ]);
        }

        return $legalSlugs;
    }
}


if(! function_exists('getCustomPages')){
    function getCustomPages(){
        $pages = Page::all()->pluck('title', 'slug');

        $array = json_decode(json_encode($pages), true);

        $customPages = [];

        foreach($array as $clave => $valor){
            array_push($customPages, (object)[
                'slug' => $clave,
                'title' => $valor
            ]);
        }

        return $customPages;
    }
}

if (! function_exists('checkCredentials')){
	function checkCredentials($jwt){
		$user = JWT::decode($jwt, env('JWT_SECRET'), array('HS256'));

		$isAdmin = User::find($user->sub);

		return $isAdmin;

	}
}

if (! function_exists('checkAuthor')){
    function checkAuthor($jwt, $id){
        $customer = JWT::decode($jwt, env('JWT_SECRET'), array('HS256'));

        return $customer->sub == $id;

    }
}