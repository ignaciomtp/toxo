<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    
	protected $fillable = [
		'user_id',
		'product_type',
		'product_id',
		'order_type',
		'shipping_method',
		'procesed',
	];


	public function customer(){
		return $this->belongsTo('App\Customer');
	}

}
