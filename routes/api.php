<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::prefix('admin')->group(function () {
	Route::post('login', 'UserController@login');

	Route::get('tariffs', 'TariffsController@tariffs');
	Route::get('tariffs/{id}', 'TariffsController@getTariff');
	Route::post('tariffs', 'TariffsController@createTariff')->middleware('jwtUserAuth');
	Route::post('tariffs/update/{id}', 'TariffsController@updateTariff')->middleware('jwtUserAuth');
	Route::delete('tariffs/delete/{id}', 'TariffsController@destroyTariff')->middleware('jwtUserAuth');

	Route::get('legaltexts', 'LegalTextsController@legalTexts');
	Route::get('legaltexts/{id}', 'LegalTextsController@getLegalText');
	Route::post('legaltexts', 'LegalTextsController@createText')->middleware('jwtUserAuth');
	Route::post('legaltexts/{id}', 'LegalTextsController@updateText')->middleware('jwtUserAuth');
	Route::delete('legaltexts/{id}', 'LegalTextsController@destroyText')->middleware('jwtUserAuth');

	Route::get('packs', 'PackController@packs');
	Route::get('packs/{id}', 'PackController@getPack');
	Route::post('packs', 'PackController@createPack')->middleware('jwtUserAuth');
	Route::post('packs/update/{id}', 'PackController@updatePack')->middleware('jwtUserAuth');
	Route::delete('packs/{id}', 'PackController@destroyPack')->middleware('jwtUserAuth');

	Route::get('posts', 'PostController@posts');
	Route::get('posts/{id}', 'PostController@getPost');
	Route::post('posts', 'PostController@createPost')->middleware('jwtUserAuth');
	Route::post('posts/update/{id}', 'PostController@updatePost')->middleware('jwtUserAuth');
	Route::delete('posts/{id}', 'PostController@destroyPost')->middleware('jwtUserAuth');	

	Route::get('offers', 'OfferController@offers');
	Route::get('offers/{id}', 'OfferController@getOffer');
	Route::post('offers', 'OfferController@createOffer')->middleware('jwtUserAuth');
	Route::post('offers/update/{id}', 'OfferController@updateOffer')->middleware('jwtUserAuth');
	Route::delete('offers/{id}', 'OfferController@destroyOffer')->middleware('jwtUserAuth');

	Route::get('customers', 'CustomerController@customers');
	Route::get('customers/{id}', 'CustomerController@getCustomer');
	Route::post('customers/update/{id}', 'CustomerController@updateCustomer')->middleware('jwtUserAuth');
	Route::delete('customers/{id}', 'CustomerController@destroyCustomer')->middleware('jwtUserAuth');

	Route::get('pages', 'PageController@pages');
	Route::get('pages/{id}', 'PageController@getPage');
	Route::post('pages', 'PageController@createPage')->middleware('jwtUserAuth');
	Route::post('pages/{id}', 'PageController@updatePage')->middleware('jwtUserAuth');
	Route::delete('pages/{id}', 'PageController@destroyPage')->middleware('jwtUserAuth');

	Route::get('carousel', 'SliderController@slides');
	Route::get('carousel/{id}', 'SliderController@getSlide');
	Route::post('carousel', 'SliderController@createSlide')->middleware('jwtUserAuth');
	Route::post('carousel/update/{id}', 'SliderController@updateSlide')->middleware('jwtUserAuth');
	Route::delete('carousel/{id}', 'SliderController@destroySlide')->middleware('jwtUserAuth');

	Route::get('users', 'UserController@users')->middleware('jwtUserAuth');
	Route::get('users/{id}', 'UserController@getUser')->middleware('jwtUserAuth');
	Route::post('users', 'UserController@createUser')->middleware('jwtUserAuth');
	Route::post('users/update/{id}', 'UserController@updateUser')->middleware('jwtUserAuth');
	Route::delete('users/{id}', 'UserController@destroyUser')->middleware('jwtUserAuth');
	Route::post('users/changepassword/{id}', 'UserController@changePassword')->middleware('jwtUserAuth');

});