<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::prefix('admin')->group(function () {
	Route::get('', 'HomeController@index')->name('home');

	Route::get('pages', ['as' => 'adminpages', 'uses' => 'HomeController@pages']);
	Route::get('pages/add', ['as' => 'addpage', 'uses' => 'HomeController@addPage']);
	Route::post('pages/add', ['as' => 'savepage', 'uses' => 'HomeController@savePage']);
});

Route::get('/', ['as' => 'inicio', 'uses' => 'PublicController@index']);
Route::get('/acerca', ['as' => 'acerca', 'uses' => 'PublicController@about']);
Route::get('/contacto', ['as' => 'contacto', 'uses' => 'PublicController@contact']);

Route::get('legal/{text}', ['as' => 'legal', 'uses' => 'PublicController@getLegalText']);

Route::get('content', ['as' => 'pages', 'uses' => 'PublicController@pages']);
Route::get('content/{page}', ['as' => 'custompage', 'uses' => 'PublicController@seePage']);

Route::get('packs', ['as' => 'packs', 'uses' => 'PublicController@packs']);
Route::get('packs/{pack}', ['as' => 'pack', 'uses' => 'PublicController@seePack']);

Route::get('noticias', ['as' => 'noticias', 'uses' => 'PublicController@allPosts']);
Route::get('noticias/{post}', ['as' => 'onepost', 'uses' => 'PublicController@getPost']);

Route::get('ofertas', ['as' => 'ofertas', 'uses' => 'PublicController@offers']);
Route::get('ofertas/{offer}', ['as' => 'oferta', 'uses' => 'PublicController@seeOffer']);

Route::get('carrito', ['as' => 'carrito', 'uses' => 'PublicController@cart']);
Route::get('carrito/add/{type}/{product}', ['as' => 'alcarrito', 'uses' => 'CartController@addToCart']);
Route::get('carrito/empty', ['as' => 'quitar', 'uses' => 'CartController@removeFromCart']);
Route::get('alta', ['as' => 'alta', 'uses' => 'PublicController@addCustomer']);
Route::post('alta', ['as' => 'crearcliente', 'uses' => 'CustomerController@createCustomer']);

Route::get('/{type}', ['as' => 'tipo', 'uses' => 'PublicController@tariffsByType']);
Route::get('/{type}/{tariff}', ['as' => 'tarifa', 'uses' => 'PublicController@seeTariff']);



