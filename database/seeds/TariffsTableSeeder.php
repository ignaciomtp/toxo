<?php

use App\Tariff;
use Illuminate\Database\Seeder;

class TariffTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        factory(Tariff::class, 20)->create();
    }
}
