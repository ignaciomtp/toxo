<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use App\Tariff;
use Faker\Generator as Faker;
use Illuminate\Support\Str;


/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'remember_token' => Str::random(10),
    ];
});


$factory->define(Tariff::class, function (Faker $faker) {
	$name = $faker->sentence($nbWords = 2, $variableNbWords = true);
	$slug = Str::slug($name, '-'); 
	$dt = $faker->dateTimeBetween($startDate = '-1 years', $endDate = 'now');
	$date = $dt->format("Y-m-d");
    return [
        'name' => $name,
        'slug' => $slug,
        'description' => $faker->text,
        'short_description' => $faker->sentence($nbWords = 10, $variableNbWords = true),
        'conditions' => $faker->sentence($nbWords = 5, $variableNbWords = true),
        'price' => $faker->numberBetween($min = 1000, $max = 100000), 
        'bandwidth' => $faker->numberBetween($min = 10, $max = 600), 
        'minutes' => $faker->numberBetween($min = 0, $max = 500),
    	'ref' => 'FIBRA300',
    	'units' => $faker->randomElement(['Gb', 'Mb']),
    	'tariff_type_id' => $faker->randomElement([1, 2, 3]),
    	'date_start' => $date,
    	'date_end' => $date,
    ];	
});
