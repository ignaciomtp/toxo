<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTariffsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tariffs', function (Blueprint $table) {
            $table->id();
            $table->string('name', 30);
            $table->string('slug', 30)->unique();
            $table->integer('tariff_type_id')->unsigned();
            $table->integer('bandwidth')->unsigned();
            $table->string('units', 2);
            $table->integer('price')->unsigned();
            $table->boolean('unlimited_calls')->default(true);
            $table->integer('minutes')->unsigned();
            $table->text('description');
            $table->string('short_description');
            $table->string('conditions', 150);
            $table->string('ref', 20);
            $table->boolean('valid')->default(true);
            $table->date('date_start');
            $table->date('date_end');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tariffs');
    }
}
