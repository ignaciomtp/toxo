<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packs', function (Blueprint $table) {
            $table->id();
            $table->string('name', 30);
            $table->string('slug', 30)->unique();
            $table->integer('mobile_tariff_id')->unsigned();
            $table->integer('adsl_tariff_id')->unsigned();
            $table->integer('fv_tariff_id')->unsigned();
            $table->integer('price')->unsigned();
            $table->text('description');
            $table->string('short_description');
            $table->string('image', 50)->nullable();
            $table->string('conditions', 150);
            $table->string('ref', 20);
            $table->boolean('valid')->default(true);
            $table->date('date_start');
            $table->date('date_end');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packs');
    }
}
