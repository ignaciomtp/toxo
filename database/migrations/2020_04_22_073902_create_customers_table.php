<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->id();
            $table->string('name', 30);
            $table->string('surname', 100);
            $table->string('form_of_address', 10);
            $table->string('email', 191)->unique();
            $table->string('password');
            $table->string('company', 50)->nullable();
            $table->string('nif', 11)->nullable();
            $table->boolean('offers')->default(false);
            $table->string('address', 100);
            $table->string('city', 50);
            $table->string('region', 50);
            $table->string('cp', 5);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
